# React POS

## Introduction
This is my pos project powered by React

In the project directory, you can run:

`npm start`

This is my POS login page
Data are dummy for testing

![Screen Shot 2565-03-08 at 21 54 08](https://user-images.githubusercontent.com/22865034/157264255-dcc505ac-1879-4a2d-bcb3-3d1ed0801849.png)

## Meta City POS selection
![Screen Shot 2565-03-08 at 21 54 28](https://user-images.githubusercontent.com/22865034/157264351-30ff52e3-ed65-4975-8e97-c9e516c52261.png)

## Main Page
![Screen Shot 2565-03-08 at 21 53 50](https://user-images.githubusercontent.com/22865034/157264329-273b75e4-5339-4dfc-877e-680671d9d132.png)

## BIlling
![Screen Shot 2565-03-08 at 21 54 55](https://user-images.githubusercontent.com/22865034/157264361-9547a613-8efa-4364-914b-ea51ad7f7409.png)

## There are many features to be implemented
