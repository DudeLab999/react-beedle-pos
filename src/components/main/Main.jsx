import React, { Component } from "react";
import './main.css';
import './modal.css';

import Header from "../header/Header";
import item1 from "../../assets/item1.png";
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleUp, faArrowLeft, faBirthdayCake, faChevronCircleLeft, faCreditCard, faGift, faMoneyBillWave, faPhoneAlt, faPrint, faQrcode, faSearch, faStar, faStore, faTag, faTags, faTimes, faTrash, faUser, faUserFriends, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import ReactToPrint from "react-to-print";

import History from "../history/History";

const CUSTOMER_RECEIPT_TITLE = "Beedle";
const TOGGLE_CHECK_ITEMS_INTERVAL = true;
const CHECK_ITEMS_INTERVAL_IN_MILLISEC = 5000;

const CAT1 = 1;
const CAT2 = 2;
const CAT3 = 3;
const CAT4 = 4;
const CAT5 = 5;

const API_URL = "http://localhost:8000";

const DISCOUNT_BILL = 1;
const DISCOUNT_CODE = 2;

export default class Main extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ...this.props.location.state,
            checkInventoryInterval: 111,
            token: "local-test",
            navActive: true,
            showOrderedList: true,
            // Customers detail
            customerName: "",
            orderedItem: [],
            // Member points
            memberPoint: 0,
            memberPointUse: 0,
            memberPointReward: 0,
            // Filter Sub-menu
            filterItemSelected: CAT1,
            paymentSelected: "cash",
            paymentMethod: "เงินสด",
            customerInput: 0,
            cashBalance: 0,
            changeAmount: 0,
            totalAmount: 0,
            // Payment
            togglePayment: false,
            confirmPayment: false,
            // Payment Success Modal
            paymentSuccess: false,
            // Register Modal
            registerModalActive: false,
            registerCurrent: true,
            registerNew: false,
            // Discount
            discountModalActive: false,
            discountType: DISCOUNT_BILL,
            discountIdSelected: 0,
            discount: 0,
            discountTypeUnit: "%",
            // POS
            posModalActive: false,

            // ModalReceipt
            modalCustomerReceiptActive: false,
            modalReceiptActive: false,

            priceSummary: 0,
            alertedOutOfStock: false,
            toggleOrderEdit: false,
            editingItem: {},
            showModalEditSuccess: false,
            menuItems: [
                {
                    "id": 1,
                    "name": "Cappuccino",
                    "type_id": 1,
                    "price": "80.00",
                    "can_craft_this_menu": 103,
                    "min_stock": 5000,
                    "onhand": "9280.00",
                    "unit": "ml",
                    "material_total": 2,
                    "img_url": "img/noimg.png"
                },
                {
                    "id": 2,
                    "name": "Latte",
                    "type_id": 3,
                    "price": "70.00",
                    "can_craft_this_menu": 20,
                    "min_stock": 10,
                    "onhand": "20.00",
                    "unit": "qty",
                    "material_total": 1,
                    "img_url": null
                },
                {
                    "id": 3,
                    "name": "Espresso",
                    "type_id": 3,
                    "price": "70.00",
                    "can_craft_this_menu": 20,
                    "min_stock": 10,
                    "onhand": "20.00",
                    "unit": "qty",
                    "material_total": 1,
                    "img_url": null
                },
                {
                    "id": 4,
                    "name": "ครัวซ็องเนยสด",
                    "type_id": 3,
                    "price": "70.00",
                    "can_craft_this_menu": 20,
                    "min_stock": 10,
                    "onhand": "20.00",
                    "unit": "qty",
                    "material_total": 1,
                    "img_url": null
                }
            ],
            discounts: [
                {
                    "id": 0,
                    "name": "ไม่มีส่วนลด",
                    "value": "0.00",
                    "type": "บาท"
                },
                {
                    "id": 1,
                    "name": "ส่วนลด 5%",
                    "value": "5.00",
                    "type": "%"
                },
                {
                    "id": 2,
                    "name": "ส่วนลด 10%",
                    "value": "10.00",
                    "type": "%"
                },
                {
                    "id": 3,
                    "name": "โปร 10 แถม 1",
                    "value": "100.00",
                    "type": "%"
                },
                {
                    "id": 4,
                    "name": "ส่วนลด 10 บาท",
                    "value": "10.00",
                    "type": "บาท"
                }
            ],
            paymentTypes: [
                {
                    "id": 1,
                    "name": "หน้าร้าน"
                },
                {
                    "id": 2,
                    "name": "Line man"
                },
                {
                    "id": 3,
                    "name": "Grab"
                }
            ],
            currentOrderNo: "",
            paymentCashType: 1,
            paymentSellWayId: 1,
            paymentMethods: [
                {
                    "id": 1,
                    "name": "เงินสด"
                },
                {
                    "id": 2,
                    "name": "พร้อมเพย์"
                },
                {
                    "id": 3,
                    "name": "True Money"
                },
                {
                    "id": 4,
                    "name": "Credit Card"
                }
            ],
        };
    }

    getMenu = () => {
        axios.get(`${API_URL}/api/v1/menu`)
            .then((res) => {
                this.setState({
                    menuItems: res.data.result
                })
            })
            .catch(err => { console.log(err); })
    }

    selectSubMenu = (category) => {
        this.setState({ filterItemSelected: category })
    }

    toggleNav = (status) => {
        this.setState({ navActive: status })
    }

    togglePaymentSuccess = () => {
        this.setState(p => ({ paymentSuccess: !p.paymentSuccess }))
    }

    clearCurrentBill = () => {
        this.setState({
            modalCustomerReceiptActive: false,
            customerName: "",
            orderedItem: [],
            memberPoint: 0,
            memberPointUse: 0,
            memberPointReward: 0,
            // Filter Sub-menu
            filterItemSelected: CAT1,
            paymentSelected: "cash",
            paymentMethod: "เงินสด",
            customerInput: 0,
            cashBalance: 0,
            changeAmount: 0,
            totalAmount: 0,
            // Payment
            togglePayment: false,
            confirmPayment: false,
            // Payment Success Modal
            paymentSuccess: false,
            // Register Modal
            registerModalActive: false,
            registerCurrent: true,
            registerNew: false,
            // Discount
            discountModalActive: false,
            discountType: DISCOUNT_BILL,
            discountIdSelected: 0,
            discount: 0,
            discountTypeUnit: "%",
            // POS
            posModalActive: false,
            // ModalReceipt
            modalReceiptActive: false,
            priceSummary: 0,
            alertedOutOfStock: false,
            toggleOrderEdit: false,
            editingItem: {},
            showModalEditSuccess: false,
            currentOrderNo: "",
            paymentCashType: 1,
            paymentSellWayId: 1,
            paymentMethods: [],
        })
    }

    addToCart = (item) => {
        let orderedItem = this.state.orderedItem;
        let latestCartItemList = [];

        let isDupplicate = false;
        orderedItem.map((oi, i) => {
            if (item.id === oi.id) {
                isDupplicate = true;
                oi.amount += 1;
            }
            return orderedItem;
        });

        latestCartItemList = orderedItem;
        if (!isDupplicate) {
            latestCartItemList.push({
                id: item.id,
                name: item.name,
                amount: 1,
                price: item.price
            });
        }
        let priceSummary = this.priceSummary();
        this.setState({ orderedItem: latestCartItemList, priceSummary: priceSummary })
    }

    toggleOrderList = () => {
        this.setState(prevState => ({ showOrderedList: !prevState.showOrderedList }))
    }

    removeBill = () => {
        let isRemoveBill = window.confirm("ท่านต้องการยกเลิก Bill ใช่หรือไม่");
        if (isRemoveBill) {
            this.clearCurrentBill();
        }
    }

    clearInput() {
        this.setState({
            customerInput: 0
        })
    }

    inputShortcut = (amount) => {
        const { customerInput } = this.state;
        this.setState({
            customerInput: customerInput === 0 || customerInput === '0' ? String(amount) : parseFloat(customerInput) + (amount)
        })
    }

    priceSummary = () => {
        // Get price from cart, ordered items
        let orderedItem = this.state.orderedItem;
        let price = 0.00;
        orderedItem.map((oi, i) => (price += parseFloat(oi.price) * parseInt(oi.amount)));
        return price;
    }

    togglePayment = () => {
        let priceSummary = this.priceSummary();
        this.setState(p => ({ togglePayment: !p.togglePayment, priceSummary: priceSummary }))

        axios.get(`${API_URL}/api/v1/paymentTypes`)
            .then((res) => {
                this.setState({
                    paymentTypes: res.data.sellWay,
                    paymentMethods: res.data.cashType
                })
            })
            .catch(err => { console.log(err); })
    }


    togglePrintCustomerReceipt = () => {
        this.setState(p => ({ modalCustomerReceiptActive: !p.modalCustomerReceiptActive }))
    }

    togglePrintReceipt = () => {
        this.setState(p => ({ modalReceiptActive: !p.modalReceiptActive }))
    }

    toggleModalOrderEditor = (item) => {
        // console.log(item);
        this.setState(p => ({ toggleOrderEdit: !p.toggleOrderEdit, editingItem: item, tempItemToUpdateAmount: item.amount }))
    }

    selectDiscountButton = (discountType) => {
        this.setState({
            discountType: discountType === DISCOUNT_BILL ? DISCOUNT_BILL : DISCOUNT_CODE,
            discountIdSelected: -1
        })
    }

    selectDiscount = (id, value, type) => {
        this.setState({ discountIdSelected: id, discount: value, discountTypeUnit: type })
    }

    applyDiscount = (id) => {
        this.setState(p => ({
            discountModalActive: !p.discountModalActive,
            discountIdSelected: id
        }))
    }

    selectPaymentMethod = (id, name) => {
        for (const payment of this.state.paymentMethods) {
            if (payment === payment.name) {
                name = payment.name;
            }
        }

        name = "ชำระด้วย " + name;
        this.setState({ paymentSelected: id, paymentMethod: name, paymentSellWay: 1 })
    }

    selectPaymentSellWay = (id, name) => {
        this.setState({ paymentSellWayId: id, paymentSellWayName: name })
    }

    inputDot() {
        const { customerInput } = this.state;

        if (!(/\./).test(customerInput)) {
            this.setState({
                customerInput: customerInput + '.'
            })
        }
    }

    inputDel() {
        const { customerInput } = this.state;

        let newInputDisplay = String(customerInput).slice(0, String(customerInput).length - 1);
        if (String(customerInput).length <= 1) {
            newInputDisplay = 0;
        }
        this.setState({
            customerInput: newInputDisplay
        })

    }

    calculator = (num) => {
        const { customerInput } = this.state;
        this.setState({
            customerInput: customerInput === 0 || customerInput === '0' ? String(num) : customerInput + String(num)
        })
    }

    confirmPayment = () => {
        const confirmPayment = window.confirm("ยืนยันการชำระเงินหรือไม่");
        this.setState({
            confirmPayment: confirmPayment,
        })
        if (confirmPayment) { this.submitOrder(); }

        console.log(this.state.orderedItem);
    }

    submitOrder = () => {
        // changeAmount
        let priceFinal = 0;
        if (this.state.discountTypeUnit === "%") {
            priceFinal = (this.state.customerInput - this.state.priceSummary) - (this.state.discount / 100);
        } else {
            priceFinal = (this.state.customerInput - this.state.priceSummary) - (this.state.discount);
        }
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };
        this.setState({ invoiceRef: "OR00001012022" });
        this.togglePaymentSuccess();
        // axios.post
        //     (
        //         `${API_URL}/api/v1/submitOrder`,
        //         {
        //             "beedle" : "eiei"
        //         },
        //         config
        //     )
        //     .then((response) => {
        //         if (response.status === 200) {
        //             if (response.data.success) {
        //                 this.setState({ invoiceRef: response.data.invoice });
        //                 this.togglePaymentSuccess();
        //             } else {
        //                 alert("ทำรายการไม่สำเร็จ กรุณาเช็คจำนวนสินค้าในคลัง");
        //             }

        //         }
        //     })
        //     .catch((error) => {
        //         if (error.response) {
        //             if (error.response.status === 401) {
        //                 // console.log(error.response.status);
        //                 alert(
        //                     `Session หมดอายุ กรุณาล็อกอินใหม่`,
        //                     `Session expired, Please login`,
        //                 );
        //             }
        //         }
        //     });
    }

    registerCurrent = () => {
        this.setState(p => ({
            registerCurrent: true,
            registerNew: false
        }))
    }

    registerNew = () => {
        this.setState(p => ({
            registerNew: true,
            registerCurrent: false,
        }))
    }

    toggleRegisterModal() {
        this.setState(p => ({
            registerModalActive: !p.registerModalActive
        }))
    }

    toggleDiscountModal() {
        const { discountModalActive } = this.state;
        try {
            if (!discountModalActive) {
                axios.get(`${API_URL}/api/v1/discounts`)
                    .then((res) => {
                        console.log(res.data.data);
                        this.setState({
                            discounts: res.data.data
                        })
                    })
                    .catch(err => { console.log(err); })
            }
        } catch (err) {
            console.log(err);
        }
        this.setState(p => ({
            discountModalActive: !p.discountModalActive
        }))
    }

    togglePOS = () => {
        this.setState(p => ({
            posModalActive: !p.posModalActive
        }))
    }

    printModal = () => {
        window.print();
    }

    updateTempItemAmount = (id, amount) => {

        this.setState((p) => ({ tempItemToUpdateId: id, tempItemToUpdateAmount: (p.tempItemToUpdateAmount + amount) < 0 ? p.tempItemToUpdateAmount : p.tempItemToUpdateAmount + amount }))
    }

    updateSelectedItem = (id, isRemove) => {
        try {
            const { orderedItem, itemsObject } = this.state;

            if (this.state.tempItemToUpdateAmount === 0 || isRemove) {
                let filteredItems = orderedItem.filter((item) => item.id !== id);
                this.setState({ orderedItem: filteredItems })
            } else {
                orderedItem.forEach(cartItem => {
                    if (cartItem.id === id) {
                        if (cartItem.amount > itemsObject[cartItem.id].available) {
                            if (!this.state.alertedOutOfStock) {
                                alert(`ขณะนี้เมนู ${cartItem.name} สามารถสั่งได้สูงสุด ${itemsObject[cartItem.id].available} กรุณาตรวจสอบรายการใหม่ก่อนยืนยัน`);
                                this.setState({ alertedOutOfStock: true });
                            }
                        } else {
                            // Update cart amount
                            cartItem.amount = this.state.tempItemToUpdateAmount;
                        }
                    }
                });
            }



        } catch (err) {

        }

        this.setState({ toggleOrderEdit: false, showModalEditSuccess: true });
    }

    isEmployeeOnline = () => {
        // window.sessionStorage.setItem("key", "value");

        let empSession = window.sessionStorage.getItem("empId");
        if (empSession != null) {
            // 
        }
    }

    componentDidMount() {
        // this.getMenu()
        if (TOGGLE_CHECK_ITEMS_INTERVAL) {
            this.setState(({
                // Get items every xx seconds, check inventory
                // checkInventoryInterval: setInterval(() => {
                //     axios.get(`${API_URL}/api/v1/menu`)
                //         .then((res) => {
                //             this.setState({
                //                 menuItems: res.data.result
                //             })
                //             try {
                //                 const { orderedItem } = this.state;
                //                 const itemsObject = {};

                //                 res.data.result.forEach(item => {
                //                     itemsObject[item.id] = {
                //                         id: item.id,
                //                         name: item.name,
                //                         price: item.price,
                //                         available: item.can_craft_this_menu,
                //                         img: item.img_url
                //                     }
                //                 });

                //                 this.setState({ itemsObject: itemsObject });

                //                 orderedItem.forEach(cartItem => {
                //                     // Just alert but no action at current
                //                     if (cartItem.amount > itemsObject[cartItem.id].available) {
                //                         if (!this.state.alertedOutOfStock) {
                //                             alert(`ขณะนี้เมนู ${cartItem.name} สามารถสั่งได้สูงสุด ${itemsObject[cartItem.id].available} กรุณาตรวจสอบรายการใหม่ก่อนยืนยัน`);
                //                             this.setState({ alertedOutOfStock: true });
                //                         }
                //                     }
                //                 });
                //             } catch (err) {

                //             }
                //         })
                //         .catch(err => { console.log(err); })

                //     axios.get(`${API_URL}/api/v1/currentInvoice`)
                //         .then((res) => {
                //             this.setState({ currentOrderNo: res.data.data });
                //         })
                //         .catch(err => { console.log(err); })

                // }, CHECK_ITEMS_INTERVAL_IN_MILLISEC)
            }))
        }

    }

    showEditSuccess = () => {
        setTimeout(() => {
            this.setState(({ showModalEditSuccess: false }))
        }, 1500);
    }

    selectNavMenu = (name) => {
        if (name !== "historyMenu") {
            this.toggleHistory(false);
        } else {
            this.toggleHistory(true);
        }
        this.setState({ navMenuSelected: name });
    }

    toggleHistory = (status) => {
        this.setState({ showHistory: status });
    }

    componentWillUnmount() {
        if (TOGGLE_CHECK_ITEMS_INTERVAL) {
            clearInterval(this.state.checkInventoryInterval);
        }

    }

    render() {
        const category = (
            <div className="top-menu">
                <div className={`recommend-menu ${this.state.filterItemSelected === CAT1 ? "active" : ""}`} onClick={() => this.selectSubMenu(CAT1)}>ชุดสุดคุ้ม</div>
                <div className={`recommend-menu ${this.state.filterItemSelected === CAT2 ? "active" : ""}`} onClick={() => this.selectSubMenu(CAT2)}>Cold Brew</div>
                <div className={`recommend-menu ${this.state.filterItemSelected === CAT3 ? "active" : ""}`} onClick={() => this.selectSubMenu(CAT3)}>Latte</div>
                <div className={`recommend-menu ${this.state.filterItemSelected === CAT4 ? "active" : ""}`} onClick={() => this.selectSubMenu(CAT4)}>Espresso</div>
                <div className={`recommend-menu ${this.state.filterItemSelected === CAT5 ? "active" : ""}`} onClick={() => this.selectSubMenu(CAT5)}>Beer</div>
            </div>
        );

        const items = (
            <div className="item-container">
                {this.state.menuItems != null && this.state.menuItems.map((item, i) => (
                    <div className="item-card" key={"item-" + i} onClick={() => this.addToCart(item)}>
                        <img className="preview-image" alt="meat" src={item1} />
                        <div className="item-detail">
                            <span>{item.name}<br />{item.price} .-</span>
                        </div>
                    </div>
                ))}
            </div>
        );

        const sidebar = (
            <div className={`sidebar`}>
                <div className="card-info">
                    <div className="profile-wrapper">
                        <div className="icon">
                            <span><FontAwesomeIcon icon={faUserPlus} color={"#D07E54"} size={"2x"} /></span>
                            <div>สมาชิก</div>
                        </div>
                        <div className="member-info">
                            <span>Beedle EiEi</span>
                            <div>
                                <span><FontAwesomeIcon icon={faPhoneAlt} color={"#D07E54"} size={"lg"} /> 098-222-2222</span>
                            </div>
                            <div>
                                <span className="text-small">CM0002021001</span>
                                <span className="member-star">1,780<FontAwesomeIcon icon={faStar} color={"#D07E54"} size={"sm"} /></span>
                            </div>
                        </div>
                    </div>
                    <div className="order-info">
                        <span>Order NO. </span><span>{this.state.currentOrderNo}</span>
                        <div>
                            <span className="small-info">{new Date().toLocaleString("th").split(" ")[0]} </span>
                            <span className="small-info">{new Date().toLocaleString("th").split(" ")[1]}</span>
                        </div>
                    </div>
                    <div className={`order-list`} >
                        <div className={`order-dropdown-button`} onClick={this.toggleOrderList}>
                            <span><FontAwesomeIcon icon={faStore} color={"#D07E54"} size={"lg"} /> ทานที่ร้าน</span>
                            <span style={{ marginLeft: '25%', marginRight: '10%' }}>
                                {
                                    this.state.showOrderedList ? <FontAwesomeIcon icon={faAngleDown} size={"sm"} /> :
                                        <FontAwesomeIcon icon={faAngleUp} size={"sm"} />
                                }
                            </span>
                        </div>
                        <div className={`ordered-container${this.state.showOrderedList ? "" : " hide"}`}>
                            {
                                this.state.orderedItem.map((item, i) => (
                                    <div className="order-row" key={`order-${i}`} onClick={() => this.toggleModalOrderEditor(item)}>
                                        <span className={`name`}>{item.name}</span>
                                        <div className="price-wrapper">
                                            <span className={`amount`}>x {item.amount}</span>
                                            <span className={`price`}>{parseFloat(item.price).toFixed(2)}</span>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>

                {/* Blliing */}
                <div className={`bill-summary`}>
                    <div className={"price-info"}>
                        <div className={"row"}>
                            <div className={"left"}>ยอดรวม</div>
                            <div className={"right"}>{this.state.priceSummary}.<span>00</span></div>
                        </div>
                        <div className={"row"}>
                            <div className={"left"}>
                                <span>ส่วนลด <span className="discount-text" onClick={() => this.toggleDiscountModal()}> เพิ่มส่วนลด</span></span>
                                <span>
                                    <FontAwesomeIcon icon={faTag} style={{ marginLeft: '3px' }} size={"sm"} color={"#D07E54"} />
                                </span>
                            </div>
                            <div className={"right"}>{this.state.discount}<span> {this.state.discountTypeUnit}</span></div>
                        </div>
                        <div className={"row"}>
                            <div className={"left"}>ภาษี</div>
                            <div className={"right"}>{this.state.priceSummary * 7 / 100}</div>
                        </div>
                        <div className={"row"}>
                            <div className={"left"}>ราคาสุทธิ</div>
                            <div className={"right"}>{this.state.priceSummary}</div>
                        </div>
                    </div>
                    <div className={"price-summary"}>
                        <div className={"row"}>
                            <div className={"left discount-text"}>Beedle Coin</div>
                            <div className={"right"}>{this.state.memberPointReward}</div>
                        </div>
                        <div className={"row"}>
                            <div className={"left"}>ยอดชำระ</div>
                            <div className={"right"}>{this.state.priceSummary}</div>
                        </div>
                        <div className={"row"}>
                            <div className={"left-bill"}>
                                <div className={"pause-bill"}>
                                    พักบิล
                                    </div>
                                <div className="bill-button-wrapper">
                                    <span onClick={() => this.removeBill()}><FontAwesomeIcon icon={faTrash} style={{ marginLeft: '3px' }} size={"lg"} color={"#D07E54"} /></span>
                                    <span className={"call-bill"}>เรียกบิล</span>
                                </div>
                            </div>
                            <div className={"check-bill"} onClick={() => this.togglePayment()}>จ่ายเงิน</div>
                        </div>
                    </div>
                </div>

            </div>
        );

        const modalEditOrder = (
            <div className={`modal__order-edit ${this.state.toggleOrderEdit ? "active" : "hide"}`}>
                <div className={`modal__order-edit-container`}>
                    <span className={`modal__order-edit-close`} onClick={() => this.setState((p) => ({ toggleOrderEdit: !p.toggleOrderEdit }))}><FontAwesomeIcon icon={faTimes} color={"#fff"} size={"lg"} /></span>
                    <div className={`modal__order-edit-title`}>ชื่อเมนู: {this.state.editingItem.name}</div>
                    <div className={`modal__order-edit-img`}>Image {this.state.editingItem.img !== "" ? this.state.editingItem.img : ""}</div>
                    <div className={`modal__order-edit-sub-title`}>จำนวน: {this.state.tempItemToUpdateAmount}</div>
                    <div className={`modal__order-edit__buttons`}>
                        <div className={`modal__order-edit__button`} onClick={() => this.updateTempItemAmount(this.state.editingItem.id, -1)}>-</div>
                        <div className={`modal__order-edit__button`} onClick={() => this.updateTempItemAmount(this.state.editingItem.id, 1)}>+</div>
                    </div>

                    <div className={`modal__order-edit__buttons-second`}>
                        <div className={`modal__order-edit__button--delete`} onClick={() => window.confirm("ยืนยันการลบรายการนี้หรือไม่") ? this.updateSelectedItem(this.state.editingItem.id, 1) : ""}>ลบรายการนี้
                            <span>
                                <FontAwesomeIcon icon={faTrash} style={{ marginLeft: '15px' }} size={"lg"} color={"#fff"} />
                            </span>
                        </div>
                        <div className={`modal__order-edit__button--confirm`} onClick={() => window.confirm("ยืนยันการแก้ไขหรือไม่") ? this.updateSelectedItem(this.state.editingItem.id, 0) : ""}>ยืนยันการแก้ไข</div>
                    </div>
                </div>
            </div>
        );

        const modalBilling = (
            <div className={`modal-billing ${this.state.togglePayment ? "active" : "hide"}`}>
                <div className={"billing-container"}>
                    <div className={"billing-column-left"}>
                        <div className={"top"}>
                            <div className="back-tab" onClick={() => this.togglePayment()}>
                                <span>
                                    <FontAwesomeIcon icon={faChevronCircleLeft} color={"#D07E54"} size={"2x"} />
                                </span>
                                <span> ย้อนกลับไปหน้าสั่งซื้อ</span>
                            </div>
                            <div className="billing-amount">
                                <span className="billing-total-text">ราคารวมทั้งหมด</span>
                                <div className="amount">
                                    <span>{this.state.priceSummary}</span><span className={"thb"}>THB</span>
                                </div>

                            </div>
                        </div>
                        <div className={"bot"}>
                            {this.state.paymentTypes !== undefined && this.state.paymentTypes.map((item, i) => {
                                return (
                                    <div key={`${item.id}-${item.name}`} className={`payment-card ${this.state.paymentSellWayId === item.id ? " active" : ""}`} onClick={() => this.selectPaymentSellWay(item.id, item.name)}>
                                        <span className={"payment-image"}>
                                            <FontAwesomeIcon icon={faCreditCard} color={"#D07E54"} size={"2x"} />
                                        </span>
                                        <span className={"payment-text"}>
                                            {item.name}
                                        </span>
                                    </div>
                                )
                            })}
                            <div className={"payment-line-break"} />
                            {this.state.paymentMethods !== undefined && this.state.paymentSellWayId === 1 && this.state.paymentMethods.map((item, i) => {
                                return (
                                    <div key={`${item.id}-paid`} className={`payment-card ${this.state.paymentSelected === item.id ? " active" : ""}`} onClick={() => this.selectPaymentMethod(item.id, item.name)}>
                                        <span className={"payment-image"}>
                                            {item.id === 1 && <FontAwesomeIcon icon={faMoneyBillWave} color={"#D07E54"} size={"2x"} />}
                                            {item.id === 2 && <FontAwesomeIcon icon={faQrcode} color={"#D07E54"} size={"2x"} />}
                                            {item.id === 3 && <FontAwesomeIcon icon={faMoneyBillWave} color={"#D07E54"} size={"2x"} />}
                                            {item.id === 4 && <FontAwesomeIcon icon={faCreditCard} color={"#D07E54"} size={"2x"} />}
                                        </span>
                                        <span className={"payment-text"}>
                                            {item.name}
                                        </span>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <div className={"billing-column-right"}>
                        <div className={"billing-input"}>
                            <div className={"label"}>
                                <span className={"label-left"}>
                                    Order NO. <span className={"small-text"}>{this.state.currentOrderNo}</span>
                                </span>
                                <span className={"label-right"}>
                                    {this.state.paymentMethod}
                                </span>
                            </div>
                            <div className={`customer-input`}>
                                <div className="amount">
                                    <input readOnly={true} type="text" name="customerInput" value={this.state.customerInput} />
                                </div>
                            </div>
                            {/* Calculator shortcut */}
                            <div className={`calculator-shortcut`}>
                                <div className="shortcut" onClick={() => this.inputShortcut(20)}>
                                    20
                                    </div>
                                <div className="shortcut" onClick={() => this.inputShortcut(50)}>
                                    50
                                    </div>
                                <div className="shortcut" onClick={() => this.inputShortcut(100)}>
                                    100
                                    </div>
                                <div className="shortcut" onClick={() => this.inputShortcut(500)}>
                                    500
                                    </div>
                                <div className="shortcut" onClick={() => this.inputShortcut(1000)}>
                                    1000
                                    </div>
                            </div>
                            {/* Calculator */}
                            <div className={`calculator`}>
                                <div className="col-1">
                                    <div className="row">
                                        <div className="numpad" onClick={() => this.calculator(7)}>
                                            <span>7</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(8)}>
                                            <span>8</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(9)}>
                                            <span>9</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="numpad" onClick={() => this.calculator(4)}>
                                            <span>4</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(5)}>
                                            <span>5</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(6)}>
                                            <span>6</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="numpad" onClick={() => this.calculator(1)}>
                                            <span>1</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(2)}>
                                            <span>2</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.calculator(3)}>
                                            <span>3</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="numpad half" onClick={() => this.calculator(0)}>
                                            <span>0</span>
                                        </div>
                                        <div className="numpad" onClick={() => this.inputDot()}>
                                            <span>.</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-3">
                                    <div className="row">
                                        <div className="numpad min" onClick={() => this.inputDel()}>
                                            <span><FontAwesomeIcon icon={faArrowLeft} color={"#D07E54"} size={"lg"} /></span>
                                        </div>
                                        <div className="numpad max" onClick={() => this.clearInput()}>
                                            <span>C</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={`confirmPaymentButton ${(this.state.priceSummary <= 0 || this.state.customerInput <= 0) ? " disable" : ""}`} onClick={() => this.confirmPayment()}>ยืนยัน</div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const modalPaymentSuccess = (
            <div className={`modal-payment-success ${this.state.paymentSuccess ? "active" : "hide"}`}>
                <div className={"modal-payment-success-container"}>
                    <div className={"customer-col"}>
                        <div className={"customer-profile-wrapper"}>
                            <div className={"title"}>ข้อมูลลูกค้า</div>
                            <div className={"profile-picture"}></div>
                            <div className={"customer-name"}>
                                {this.state.customerName}
                            </div>
                        </div>
                        <div className={"customer-coupon-wrapper"}>
                            <div className={"row-2"}>
                                <span className={"col-1"}>ราคาทั้งหมด</span>
                                <span className={"col-2"}>{this.state.priceSummary}</span>
                            </div>
                            <div className={"row-2"}>
                                <span className={"col-1"}>ใช้แต้มสะสมไป</span>
                                <span className={"col-2"}>{this.state.memberPointUse}</span>
                            </div>
                            <div className={"row-2"}>
                                <span className={"col-1"}>แต้มสะสมจากการซื้อครั้งนี้</span>
                                <span className={"col-2"}>{this.state.memberPointReward}</span>
                            </div>
                            <div className={"row-2"}>
                                <span className={"col-1"}>แต้มสะสมคงเหลือ</span>
                                <span className={"col-2"}><span style={{ marginRight: '10px' }}>{this.state.memberPoint}</span> <FontAwesomeIcon icon={faStar} color={"#D07E54"} size={"sm"} /></span>
                            </div>
                        </div>
                    </div>
                    <div className={"info-col"}>
                        <div className={"bill-top"}>
                            <div className={"title"}>การชำระเงินเสร็จสิ้น</div>
                            <div className={"row-2"}>
                                <span className={"col-1"}>ช่องทางการชำระ</span>
                                <span className={"col-2"}>{this.state.paymentMethod} </span>
                            </div>
                            <div className={"row-2"}>
                                <span className={"col-1"}>เลขที่ใบเสร็จ</span>
                                <span className={"col-2"}>{this.state.invoiceRef} </span>
                            </div>
                        </div>
                        <div className={"bill-summary"}>
                            <div className={"row-3"}>
                                <span className={"col-1"}>ราคาทั้งหมด</span>
                                <span className={"col-2"}>{this.state.priceSummary}</span>
                                <span className={"col-3"}>THB</span>
                            </div>
                            <div className={"row-3"}>
                                <span className={"col-1"}>ส่วนลดทั้งหมด</span>
                                <span className={"col-2"}>{this.state.discount}</span>
                                <span className={"col-3"}>THB</span>
                            </div>
                            <div className={"row-3"}>
                                <span className={"col-1"}>ราคาสุทธิ</span>
                                <span className={"col-2"}>{this.state.priceSummary}</span>
                                <span className={"col-3"}>THB</span>
                            </div>
                            <div className={"row-3"}>
                                <span className={"col-1"}>จำนวนที่ชำระ</span>
                                <span className={"col-2"}>{this.state.customerInput}</span>
                                <span className={"col-3"}>THB</span>
                            </div>
                            <div className={"row-3"}>
                                <span className={"col-1"}>ทอน</span>
                                <span className={"col-2 lg"}>{this.state.customerInput - this.state.priceSummary}</span>
                                <span className={"col-3"}>THB</span>
                            </div>

                            <div className={"print-button"} onClick={() => this.togglePrintCustomerReceipt()}>
                                <span><FontAwesomeIcon icon={faPrint} color={"#fffff"} size={"lg"} /> Print</span>
                            </div>
                            <div className={"clear-customer-button"} onClick={() => this.clearCurrentBill()}>
                                <span>เริ่มต้นขายใหม่</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const modalMemberRegister = (
            <div className={`modal-register ${this.state.registerModalActive ? "active" : "hide"}`}>
                <div className={"modal-register-container"}>
                    <div className={"top-bar-wrapper"}>
                        <div className={"register-menu"}>
                            <div className={"users-icon"}>
                                <FontAwesomeIcon icon={faUserFriends} color={"#D07E54"} size={"2x"} />
                            </div>
                            <div className={`register-button ${this.state.registerCurrent ? "active" : ""}`} onClick={() => { this.registerCurrent() }}>
                                เพิ่มสมาชิกบิลนี้
                                </div>

                            <div className={`register-button ${this.state.registerNew ? "active" : ""}`} onClick={() => { this.registerNew() }}>
                                สมัครสมาชิกใหม่
                                </div>

                        </div>

                        <div className={"close-menu-wrapper"}>
                            <div className={"close-button"} onClick={() => this.toggleRegisterModal()}>
                                <FontAwesomeIcon icon={faTimes} color={"#ededed"} size={"lg"} />
                            </div>
                        </div>
                    </div>

                    <div className={`register-form-wrapper ${this.state.registerCurrent ? "active" : "hide"}`}>
                        <div className={"input-block"}>
                            <div className={"label"}>
                                ชื่อลูกค้า
                                </div>
                            <div className={"register-input"}>
                                <input type="text" name="register-name" />
                            </div>
                        </div>

                        <div className={"input-block"}>
                            <div className={"label"}>
                                เบอร์โทรศัพท์
                                </div>
                            <div className={"register-input"}>
                                <input type="text" name="register-phone" />
                            </div>
                        </div>
                        <div className={"input-block-user"}>
                            <div className={"search-user"}>
                                <span>ค้นหา <FontAwesomeIcon icon={faSearch} color={"#ffffff"} size={"lg"} /></span>
                            </div>
                        </div>
                    </div>

                    <div className={`register-info-wrapper ${this.state.registerCurrent ? "active" : "hide"}`}>
                        <div className={"img-col"}>
                            <img className="img-cover" src="" alt="cover" />
                        </div>
                        <div className={"register-info-col"}>
                            <div className={"register-row"}>
                                <div className={"row-title"}>
                                    <span><FontAwesomeIcon icon={faUser} color={"#D07E54"} size={"lg"} /> รหัสลูกค้า</span>
                                </div>
                                <div className={"row-detail"}>
                                    <span>C10163070052</span>
                                </div>
                            </div>
                            <div className={"register-row"}>
                                <div className={"row-title"}>
                                    <span><FontAwesomeIcon icon={faTag} color={"#D07E54"} size={"lg"} /> ชื่อ - นามสกุล</span>
                                </div>
                                <div className={"row-detail"}>
                                    <span>โชคดี สุดๆ</span>
                                </div>
                            </div>
                            <div className={"register-row"}>
                                <div className={"row-title"}>
                                    <span><FontAwesomeIcon icon={faPhoneAlt} color={"#D07E54"} size={"lg"} /> เบอร์โทรศัพท์</span>
                                </div>
                                <div className={"row-detail"}>
                                    <span>091-123-4567</span>
                                </div>
                            </div>
                            <div className={"register-row"}>
                                <div className={"row-title"}>
                                    <span><FontAwesomeIcon icon={faBirthdayCake} color={"#D07E54"} size={"lg"} /> วันเกิด</span>
                                </div>
                                <div className={"row-detail"}>
                                    <span>01/01/2533</span>
                                </div>
                            </div>
                            <div className={"register-row"}>
                                <div className={"row-title"}>
                                    <span><FontAwesomeIcon icon={faGift} color={"#D07E54"} size={"lg"} /> คะแนนคงเหลือ</span>
                                </div>
                                <div className={"row-detail"}>
                                    <span>700</span>
                                </div>
                            </div>

                            <div className={"register-row"}>
                                <div className={"register-form-button"}>
                                    ยกเลิก
                                    </div>
                                <div className={"register-form-button focus"}>
                                    เพิ่มสมาชิก
                                    </div>
                            </div>

                        </div>
                    </div>

                    {/* Register new user */}
                    <div className={`register-new-wrapper ${this.state.registerNew ? "active" : "hide"}`}>
                        <div className={"row"}>
                            <div className={"input-block"}>
                                <div className={"label"}>
                                    ชื่อ - นามสกุล
                                </div>
                                <div className={"register-input"}>
                                    <input type="text" name="register-name" />
                                </div>
                            </div>

                            <div className={"input-block"}>
                                <div className={"label"}>
                                    เบอร์โทรศัพท์
                                </div>
                                <div className={"register-input"}>
                                    <input type="text" name="register-name" />
                                </div>
                            </div>
                        </div>

                        <div className={"row"}>
                            <div className={"input-block"}>
                                <div className={"label"}>
                                    เพศ
                                    </div>
                                <div className={"register-input"}>
                                    <select value={this.state.value} onChange={this.handleChange}>
                                        <option value="male">ชาย</option>
                                        <option value="female">หญิง</option>
                                        <option value="lgbt">LGBT</option>
                                    </select>
                                </div>
                            </div>

                            <div className={"input-block"}>
                                <div className={"label"}>
                                    วัน/เดือน/ปีเกิด
                                    </div>
                                <div className={"register-input"}>
                                    <input type="text" name="register-name" placeholder="DD/MM/YYYY" />
                                </div>
                            </div>
                        </div>

                        <div className={"register-row"}>
                            <div className={"register-form-button"}>
                                ยกเลิก
                                    </div>
                            <div className={"register-form-button focus"}>
                                สมัครสมาชิกใหม่
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const modalDiscount = (
            <div className={`modal-discount ${this.state.discountModalActive ? "active" : "hide"}`}>
                <div className={"modal-discount-container"}>
                    <div className={"close-menu-wrapper"}>
                        <div className={"close-button"} onClick={() => this.toggleDiscountModal()}>
                            <FontAwesomeIcon icon={faTimes} color={"#ededed"} size={"lg"} />
                        </div>
                    </div>
                    <div className={"discount-selector-wrapper"}>
                        <div className={"title"}>
                            ส่วนลด
                        </div>
                        <div className={"discount-selector"}>
                            <div className={`register-form-button ${this.state.discountType === DISCOUNT_BILL ? "focus" : ""}`} onClick={() => this.selectDiscountButton(DISCOUNT_BILL)}>
                                ส่วนลดท้ายบิล
                            </div>
                            <div className={`register-form-button ${this.state.discountType === DISCOUNT_CODE ? "focus" : ""}`} onClick={() => this.selectDiscountButton(DISCOUNT_CODE)}>
                                โค้ดส่วนลด
                            </div>
                        </div>

                    </div>

                    {/* discount content */}
                    <div className={`discount-bill ${this.state.discountType === DISCOUNT_BILL ? "active" : "hide"}`}>
                        <div className={"discount-type"}>
                            ส่วนลดเปอร์เซ็นต์ %
                        </div>
                        <div className={"discount-items-wrapper"}>
                            {
                                this.state.discounts !== undefined && this.state.discounts.map((item) => {
                                    if (item.type === "%") return (
                                        <div className={`discount-item ${this.state.discountIdSelected === item.id ? "focus" : ""}`} onClick={() => this.selectDiscount(item.id, item.value, item.type)} key={`did-${item.id}`}>
                                            <div className={"title"}>{item.name}</div>
                                            <div className={`percent`}>{item.value}{item.type === "%" ? "%" : " บาท"}</div>
                                        </div>
                                    );
                                    return "";
                                }
                                )
                            }
                        </div>


                        {/* Discount cash */}
                        <div className={"discount-type"}>
                            ส่วนลดเงินสด
                        </div>
                        <div className={"discount-items-wrapper"}>
                            {
                                this.state.discounts !== undefined && this.state.discounts.map((item) => {
                                    if (item.type !== "%") return (
                                        <div className={`discount-item ${this.state.discountIdSelected === item.id ? "focus" : ""}`} onClick={() => this.selectDiscount(item.id, item.value, item.type)} key={`did-${item.id}`}>
                                            <div className={"title"}>{item.name}</div>
                                            <div className={`percent`}>{item.value}{item.type === "%" ? "%" : " บาท"}</div>
                                        </div>
                                    );
                                    return "";
                                }
                                )
                            }
                        </div>
                    </div>

                    <div className={`discount-code ${this.state.discountType === DISCOUNT_CODE ? "active" : "hide"}`}>
                        <div className={"discount-code-wrapper"}>
                            <FontAwesomeIcon icon={faTags} color={"#D07E54"} size={"lg"} />
                            <input type="text" name="discount-search" placeholder="โปรดระบุโค้ดส่วนลด" />
                            <div className={"discount-search"}>
                                ค้นหา <FontAwesomeIcon style={{ marginLeft: '5px' }} icon={faSearch} color={"#ffffff"} size={"sm"} />
                            </div>
                        </div>

                        <div className={"discount-info-wrapper"}>
                            <div className={"row"}>
                                <div className={"title"}>ชื่อส่วนลดท้ายบิล</div>
                                <div className={"detail"}>ส่วนลดสำหรับแขก</div>
                            </div>
                            <div className={"row"}>
                                <div className={"title"}>ส่วนลด</div>
                                <div className={"detail amount"}>200.00</div>
                            </div>
                        </div>
                    </div>


                    {/* Discount buttons */}
                    <div className={"register-row"}>
                        <div className={"register-form-button"} onClick={() => this.toggleDiscountModal()}>
                            ยกเลิก
                        </div>
                        <div className={"register-form-button focus"} onClick={() => this.applyDiscount(this.state.discountIdSelected)}>
                            ใช้ส่วนลด
                        </div>
                    </div>

                </div>
            </div>
        );

        const modalEndPOS = (
            <div className={`pos-modal-container ${this.state.posModalActive ? "active" : "hide"}`}>
                <div className="modal-content">
                    <div className="close">
                        <span onClick={() => this.togglePOS()}>&times;</span>
                    </div>
                    <div className="pos-form-wrapper">
                        <div className="modal__column__left">
                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <div className="modal__card__title">การปิดยอด</div>
                                </div>
                                <div className="modal__card__row-wrapper">
                                    <div className="modal__card__title">POS {this.state.showPosId}</div>
                                </div>
                            </div>
                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">พนักงาน: </span>
                                    <span className="text-right">{this.state.firstname} {this.state.lastname}</span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">เวลาเริ่มขาย: </span>
                                    <span className="text-right">{this.state.startPOSDateTime}</span>
                                </div>
                            </div>


                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">เวลาสิ้นสุด: </span>
                                    <span className="text-right">{new Date().toLocaleString()}</span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">ระยะเวลาขาย: </span>
                                    <span className="text-right">{new Date().toLocaleString() - this.state.startPOSDateTime !== undefined ? this.state.startPOSDateTime : 0}</span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">เงินสดตั้งต้น: </span>
                                    <span className="text-right">{this.state.initialBudget}</span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">ยอดขาย: </span>
                                    <span className="text-right">-</span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">เงินสดปิดยอด: </span>
                                    <span className="text-right">-</span>
                                </div>
                            </div>
                        </div>

                        <div className="modal__column__right">

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <div className="modal__card__title">ตัวช่วยนับเงิน</div>
                                </div>
                            </div>
                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src="" alt="thb-cash" />
                                            <span className="modal-amount-text"> 1000</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="thousands" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.thousands} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 500</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fivehundreds" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fivehundreds} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>


                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 100</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="onehundreds" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.onehundreds} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 50</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fifty" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fifty} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 20</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="twenty" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.twenty} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 10</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="tens" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.tens} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 5</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fives" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fives} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="modal__card__row">
                                <div className="modal__card__row-wrapper">
                                    <span className="text-left">
                                        <div className="column-bg">
                                            <img className="modal-icon" src={""} alt="thb-cash" />
                                            <span className="modal-amount-text"> 1</span>
                                        </div>
                                    </span>

                                    <span className="text-right">
                                        <div className="column-bg-right">
                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="ones" defaultValue={0} /></span>
                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.ones} readOnly={true} /></span>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className="budget-total"><span>Total</span> <input className="budget-total-input" type="number" name="budget-total-input" value={this.state.totalBudget !== undefined ? parseInt(this.state.totalBudget) : 0} readOnly={true} /></div>
                        </div>
                    </div>

                    {/* Payment summary POS */}
                    <div className={"pos-summary-bottom"}>
                        <div style={{ marginTop: '20px' }}>ช่องทางการชำระ :</div>
                        <div className={"column-wrapper"}>

                            <div className={"col"}>
                                <div className="modal__card__row">
                                    <div className="modal__card__row-wrapper">
                                        <span className="text-left">เงินสด </span>
                                        <span className="text-right focus">{this.state.initialBudget}</span>
                                    </div>
                                </div>
                                <div className="modal__card__row">
                                    <div className="modal__card__row-wrapper">
                                        <span className="text-left">พร้อมเพย์ </span>
                                        <span className="text-right focus">{this.state.initialBudget}</span>
                                    </div>
                                </div>
                            </div>

                            <div className={"col"}>
                                <div className="modal__card__row__summary">
                                    <div className="modal__card__row-wrapper">
                                        <span className="text-left">ยอดนับเงินสด </span>
                                        <span className="text-right border">{this.state.initialBudget}</span>
                                    </div>
                                </div>
                                <div className="modal__card__row__summary">
                                    <div className="modal__card__row-wrapper">
                                        <span className="text-left">Diff </span>
                                        <span className="text-right border">{this.state.initialBudget}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={"modal__bottom__buttons"}>
                        <div className={"modal__button button--danger"}>
                            ปิดยอด
                        </div>
                        <div className={"modal__button button--focus"}>
                            ขายต่อ
                        </div>
                    </div>
                </div>
            </div>
        );

        const modalCustomerReceipt = (
            <div className={`modal__receipt ${this.state.modalCustomerReceiptActive ? "active" : "hide"}`} onClick={() => this.togglePrintCustomerReceipt()}>
                <div className={"modal__receipt-container printable"} ref={el => (this.componentRef = el)}>
                    <div className={"modal__receipt-title"}>
                        {CUSTOMER_RECEIPT_TITLE}
                    </div>
                    <div className={"modal__receipt-sub-title"}>
                        {`TAX#${this.state.invoiceRef} (VAT Included)`}
                    </div>
                    <div className={"modal__receipt-sub-title"}>
                        {`Vat xx00 POS#: 01`}
                    </div>
                    <div className={"modal__receipt-sub-title"}>
                        {`ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ`}
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>POS ID :</div>
                        <div className={"modal__receipt-row__right"}>POS 1</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ชื่อพนักงาน :</div>
                        <div className={"modal__receipt-row__right"}>จอห์น ชาวไร่</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>เวลาเริ่มขาย :</div>
                        <div className={"modal__receipt-row__right"}>05/12/2021 12:20</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>เวลาสิ้นสุด:</div>
                        <div className={"modal__receipt-row__right"}>05/12/2021 20:20</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ระยะเวลาขาย :</div>
                        <div className={"modal__receipt-row__right"}>08:00:00</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ยอดรวม :</div>
                        <div className={"modal__receipt-row__right"}>{this.state.priceSummary}</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>Vat 7%:</div>
                        <div className={"modal__receipt-row__right"}>{this.state.priceSummary * 7 / 100}</div>
                    </div>
                    <div className={"modal__receipt-close-line"} />


                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>Thankyou</div>
                    </div>

                    <div className={"modal__print-button no-print"} onClick={() => this.printModal()}>
                        <ReactToPrint
                            trigger={() => <a href="#">Print</a>}
                            content={() => this.componentRef}
                        />
                    </div>
                </div>
            </div>
        );

        const modalPOSReceipt = (
            <div className={`modal__receipt ${this.state.modalReceiptActive ? "active" : "hide"}`} onClick={() => this.togglePrintReceipt()}>
                <div className={"modal__receipt-container printable"} ref={el => (this.componentRef = el)}>
                    <div className={"modal__receipt-title"}>
                        สรุปการขายสิ้นวัน
                </div>
                    <div className={"modal__receipt-sub-title"}>
                        5/12/2021
                </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>POS ID :</div>
                        <div className={"modal__receipt-row__right"}>POS 1</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ชื่อพนักงาน :</div>
                        <div className={"modal__receipt-row__right"}>จอห์น ชาวไร่</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>เวลาเริ่มขาย :</div>
                        <div className={"modal__receipt-row__right"}>05/12/2021 12:20</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>เวลาสิ้นสุด:</div>
                        <div className={"modal__receipt-row__right"}>05/12/2021 20:20</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ระยะเวลาขาย :</div>
                        <div className={"modal__receipt-row__right"}>08:00:00</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>จำนวนบิลขาย :</div>
                        <div className={"modal__receipt-row__right"}>305</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>จำนวนบิล Void :</div>
                        <div className={"modal__receipt-row__right"}>0</div>
                    </div>
                    <div className={"modal__receipt-close-line"} />


                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left"}>ช่องทางการชำระ :</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left modal__row--indent"}>เงินสด:</div>
                        <div className={"modal__receipt-row__right modal__text--md"}>10,000</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left modal__row--indent"}>พร้อมเพย์:</div>
                        <div className={"modal__receipt-row__right modal__text--md"}>20,000</div>
                    </div>
                    <div className={"modal__receipt-close-line"} />
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left modal__row--indent"}>ยอดนับเงินสด</div>
                        <div className={"modal__receipt-row__right modal__text--md"}>30,000</div>
                    </div>
                    <div className={"modal__receipt-row"}>
                        <div className={"modal__receipt-row__left modal__row--indent"}>Diff</div>
                        <div className={"modal__receipt-row__right modal__text--md"}>0</div>
                    </div>

                    <div className={"modal__print-button no-print"} onClick={() => this.printModal()}>
                        <ReactToPrint
                            trigger={() => <a href="#">Print</a>}
                            content={() => this.componentRef}
                        />
                    </div>
                </div>
            </div>
        );

        const modalEditPrompt = (
            <div className={`modal__order-edit-prompt ${this.state.showModalEditSuccess ? "active" : "hide"} `}>
                แก้ไขรายการเรียบร้อยแล้ว
                {this.state.showModalEditSuccess ? this.showEditSuccess() : ""}
            </div>
        );

        if (this.state.showHistory) {
            return <History showHistory={(status) => this.toggleHistory(status)} />
        } else {
            return (
                <div className="main-layout">
                    <Header navActive={(status) => this.toggleNav(status)} selectNavMenu={(name) => this.selectNavMenu(name)} showHistory={(status) => this.toggleHistory(status)} />
                    <div className="title-bar">
                        <span>คิว A: A0001</span>
                        <span>คิว B: B0005</span>
                        <span>POS1</span>
                        <div className={"search-menu"}>
                            <input type="text" name="item-filter" placeholder={"ค้นหาสินค้า"} />
                            <span>
                                <FontAwesomeIcon icon={faSearch} style={{ marginLeft: '3px' }} size={"sm"} color={"#a7a7a7"} />
                            </span>
                        </div>
                    </div>
                    <div className={`main-container${!this.state.navActive ? " stretch" : ""}`}>
                        {category}
                        <hr className="hr-card" />
                        {items}
                    </div>

                    {sidebar}
                    {/* Modal edit order */}
                    {this.state.showModalEditSuccess ? modalEditPrompt : ""}

                    {this.state.toggleOrderEdit ? modalEditOrder : ""}

                    {/* End */}

                    {/* Modal Billing */}
                    {this.state.togglePayment ? modalBilling : ""}

                    {/* Modal Payment success */}
                    {this.state.paymentSuccess ? modalPaymentSuccess : ""}

                    {/* Modal Member Register */}
                    {this.state.registerModalActive ? modalMemberRegister : ""}

                    {/* Modal Discount */}
                    {this.state.discountModalActive ? modalDiscount : ""}

                    {/* Modal End POS */}
                    {this.state.posModalActive ? modalEndPOS : ""}

                    {/* Modal customer receipt */}
                    {this.state.modalCustomerReceiptActive ? modalCustomerReceipt : ""}

                    {/* Modal receipt */}
                    {this.state.modalReceiptActive ? modalPOSReceipt : ""}

                </div>
            )
        }

    }
}