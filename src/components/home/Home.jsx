import React, { Component } from "react";
import { Redirect } from "react-router";
import './home.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoins, faMoneyBillWave } from "@fortawesome/free-solid-svg-icons";

export default class Home extends Component {
    constructor(props) {
        super(props)

        let nullState = false;
        let sessionState = null;

        this.state = {
            sessionExpired: false,
            username: nullState ? sessionState.username : this.props.location.state.username,
            password: nullState ? sessionState.password : this.props.location.state.password,
            firstname: nullState ? sessionState.firstname : this.props.location.state.firstname,
            lastname: nullState ? sessionState.lastname : this.props.location.state.lastname,
            employeeActive: false,

            thousands: 0,
            fivehundreds: 0,
            onehundreds: 0,
            fifty: 0,
            twenty: 0,
            tens: 0,
            fives: 0,
            ones: 0,
            initialBudget: 0,
            totalBudget: 0,

            // POS data
            branches: [
                {
                    name: "Meta City",
                    posCount: 3,
                    posStatus: ["active", "active", "inactive"],
                    posDetail: [
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        },
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        },
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        }
                    ]
                },
                {
                    name: "Meta City 2",
                    posCount: 3,
                    posStatus: ["active", "active", "inactive"],
                    posDetail: [
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        },
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        },
                        {
                            posId: 1,
                            employeeName: "-",
                            startDateTime: "-",
                            endDateTime: "-",
                            duration: "-",
                            posStatus: "inactive"
                        }
                    ]
                },
            ]
        }

    }

    showModal = (branchName, posId, status) => {
        if (status === "active" || this.state.employeeActive) {

        } else {
            this.setState({ showModal: true, showBranch: branchName, showPosId: posId })
        }
    }

    toggleModal = () => {
        this.setState(prevState => ({ showModal: !prevState.showModal }))
    }

    inputBudgetHandle = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let calculatedValue = 0;

        let budget = this.state.totalBudget;
        let oldValue = this.state[name];

        switch (name) {
            case "thousands":
                calculatedValue = value * 1000;
                break;
            case "fivehundreds":
                calculatedValue = value * 500;
                break;
            case "onehundreds":
                calculatedValue = value * 100;
                break;
            case "fifty":
                calculatedValue = value * 50;
                break;
            case "twenty":
                calculatedValue = value * 20;
                break;
            case "tens":
                calculatedValue = value * 10;
                break;
            case "fives":
                calculatedValue = value * 5;
                break;
            case "ones":
                calculatedValue = value * 1;
                break;
            case "initialBudget":
                calculatedValue = value * 1;
                break;
            default:
                break;
        }

        // Subtract old value before add new value
        budget = (budget - oldValue) + calculatedValue;


        this.setState({
            [name]: calculatedValue,
            totalBudget: budget
        });

    }

    beginPos = () => {
        // Check input
        if (this.state.totalBudget > 0 && this.state.employeeActive === false) {
            // Ok
            this.toggleModal();

            // Sent data to server
            // Re render POS state
            this.setState({
                employeeActive: true,
                startPOSDateTime: new Date().toLocaleString()
            });

        } else {
            alert("กรุณากรอกข้อมูลก่อนเริ่มใช้งาน")
        }

    }

    componentDidMount() {
        window.onbeforeunload = function () {
            return true;
        };
    }

    componentWillUnmount() {
        window.onbeforeunload = null;
    }

    render() {
        if (this.state.sessionExpired) {
            alert("Session หมดอายุ กรุณาล็อกอินใหม่");
            return <Redirect to={{ pathname: "/" }} />
        }
        if (this.state.employeeActive) {
            return <Redirect to={{
                pathname: "/main",
                state: { from: "home", username: this.state.username, password: this.state.password, firstname: this.state.firstname, lastname: this.state.lastname, totalBudget: this.state.totalBudget, startPOSDateTime: this.state.startPOSDateTime, initialBudget: this.state.totalBudget }
            }} />
        } else
            return (
                <div className="home">
                    {/* Pop Up */}
                    {this.state.showModal &&
                        (
                            <div className="pos-modal-container" key={"modal"}>
                                <div className="modal-content">
                                    <div className="close">
                                        <span onClick={() => this.toggleModal()}>&times;</span>
                                    </div>
                                    <div className="pos-form-wrapper">
                                        <div className="column">

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <div className="modal-title">POS {this.state.showPosId}</div>
                                                </div>
                                            </div>
                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">พนักงาน: </span>
                                                    <span className="text-right">{this.state.firstname} {this.state.lastname}</span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">เวลาเริ่มขาย: </span>
                                                    <span className="text-right">{new Date().toLocaleString()}</span>
                                                </div>
                                            </div>


                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">เวลาสิ้นสุด: </span>
                                                    <span className="text-right">-</span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">ระยะเวลาขาย: </span>
                                                    <span className="text-right">-</span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">เงินสดตั้งต้น: </span>
                                                    <span className="text-right"><input className="initialBudget" name="initialBudget" onChange={this.inputBudgetHandle} type="number" value={this.state.initialBudget} /></span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">ยอดขาย: </span>
                                                    <span className="text-right">-</span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">เงินสดปิดยอด: </span>
                                                    <span className="text-right">-</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="column">

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <div className="modal-title">ตัวช่วยนับเงิน</div>
                                                </div>
                                            </div>
                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faMoneyBillWave} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 1000</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="thousands" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.thousands} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faMoneyBillWave} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 500</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fivehundreds" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fivehundreds} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>


                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faMoneyBillWave} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 100</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="onehundreds" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.onehundreds} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faMoneyBillWave} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 50</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fifty" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fifty} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faMoneyBillWave} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 20</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="twenty" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.twenty} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faCoins} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 10</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="tens" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.tens} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faCoins} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 5</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="fives" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.fives} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="home__card-row">
                                                <div className="home__card-row-wrapper">
                                                    <span className="text-left">
                                                        <div className="column-bg">
                                                            <div className="modal-icon">
                                                                <FontAwesomeIcon icon={faCoins} color={"#fff"} size={"lg"} />
                                                            </div>
                                                            <span className="modal-amount-text"> 1</span>
                                                        </div>
                                                    </span>

                                                    <span className="text-right">
                                                        <div className="column-bg-right">
                                                            <span className="column-bg-left"><input className="column-input" type="number" onChange={this.inputBudgetHandle} name="ones" defaultValue={0} /></span>
                                                            <span className="modal-amount-input"><input className="column-input-summary" type="number" value={this.state.ones} readOnly={true} /></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="budget-total"><span>Total</span> <input className="budget-total-input" type="number" name="budget-total-input" value={parseInt(this.state.totalBudget)} readOnly={true} /></div>
                                        </div>
                                    </div>

                                    <div className="start-button" onClick={this.beginPos}>เริ่มต้นขาย</div>
                                </div>
                            </div>
                        )
                    }
                    <div className="home__container">
                        {
                            this.state.branches.map((e, i) => {
                                return (
                                    e.posDetail.map((pos, id) => {
                                        return (
                                            <div className="home__card-container" key={`branch-${e.name}-${id}`}>
                                                <div className="home__card-title">สาขา {e.name}</div>
                                                <hr className="hr-line" />
                                                <div className="home__card-wrapper" >
                                                    <div className="home__card-header">POS{pos.posId}</div>
                                                    <div className="home__card-row">
                                                        <div className="home__card-row-wrapper">
                                                            <span className="text-left">พนักงาน: </span>
                                                            <span className="text-right">{pos.employeeName}</span>
                                                        </div>
                                                    </div>

                                                    <div className="home__card-row">
                                                        <div className="home__card-row-wrapper">
                                                            <span className="text-left">เวลาเริ่มขาย: </span>
                                                            <span className="text-right">{pos.startDateTime}</span>
                                                        </div>
                                                    </div>


                                                    <div className="home__card-row">
                                                        <div className="home__card-row-wrapper">
                                                            <span className="text-left">เวลาสิ้นสุด: </span>
                                                            <span className="text-right">{pos.endDateTime}</span>
                                                        </div>
                                                    </div>

                                                    <div className="home__card-row">
                                                        <div className="home__card-row-wrapper">
                                                            <span className="text-left">ระยะเวลาขาย: </span>
                                                            <span className="text-right">{pos.duration}</span>
                                                        </div>
                                                    </div>
                                                    <hr className="hr-card" />
                                                    <div className="pos-status" onClick={() => { this.showModal(e.name, pos.posId, pos.posStatus) }}>
                                                        <span className={pos.posStatus === "active" ? "pos-active" : "pos-inactive"}>{pos.posStatus === "active" ? "Active" : "-"}</span>
                                                    </div>

                                                </div>
                                            </div>
                                        )
                                    })

                                )
                            }
                            )
                        }
                    </div>
                </div>
            );

    }

}