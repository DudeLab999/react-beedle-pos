export { default as Header } from './header/Header';
export { default as Home } from './home/Home';
export { default as Login } from './login/Login';
export { default as Main } from './main/Main';