import React, { Component } from "react";
import './login.css';
import logo from '../../assets/logo.png';
import bg from '../../assets/bg3.png';

import { Redirect } from "react-router";

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            login: false,
            alertMessage: "กรุณาเข้าใช้งานด้วยรหัสผ่านของท่าน"
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    submit = () => {
        this.checkLogin()
    }

    checkLogin = () => {
        if (this.state.username === undefined || this.state.username == null
            || this.state.password === undefined || this.state.password == null) {
            this.setState({
                alertMessage: "กรุณากรอก username/password ให้ครบถ้วน"
            });
        } else {
            // Verify
            // TODO Replace with real login api
            this.setState({
                alertMessage: "กรุณาเข้าใช้งานด้วยรหัสผ่านของท่าน",
                login: true,
                firstname: "John",
                lastname: "Cena"
            });
        }
    }

    render() {
        if (this.state.login) {
            // TODO encrypt personal info
            window.sessionStorage.setItem("login", JSON.stringify({ from: "login", username: this.state.username, password: this.state.password, firstname: this.state.firstname, lastname: this.state.lastname }));

            return <Redirect to={{
                pathname: "/home",
                state: { from: "login", username: this.state.username, password: this.state.password, firstname: this.state.firstname, lastname: this.state.lastname }
            }} />
        } else {
            return (
                <div className="beedle__login">
                    <div className="beedle__login-bg">
                        <img src={bg} alt="bg" />
                    </div>
                    <div className="beedle__login-container">
                        <div className="beedle__login-container__logo">
                            <img alt="logo" src={logo} />
                        </div>
                        <label>ลงชื่อเข้าสู่ระบบ</label>
                        <div className="beedle__login-container__form">
                            <input name="username" type="text" onChange={this.handleInputChange} />
                            <input name="password" type="password" onChange={this.handleInputChange} />
                        </div>
                        <div className="beedle__login-container__submit">
                            <div className="beedle__login-container__submit-text-alert">{this.state.alertMessage}</div>
                            <div className="beedle__login-container__submit-login-button" onClick={this.submit}>
                                <div>เข้าสู่ระบบ</div>
                            </div>
                        </div>
                    </div>

                </div>
            );
        }

    }

}