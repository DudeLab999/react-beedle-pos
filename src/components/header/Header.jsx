import '../../App.css';
import './header.css';
import React, { useState } from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faHistory, faMobileAlt, faThLarge } from '@fortawesome/free-solid-svg-icons'

const Header = (props) => {
    const [navActive, toggleNavbar] = useState(true);
    const [menuActive, setMenuActive] = useState("mainMenu");

    return (
        <>
            <div className={`navbar${navActive ? " active" : ""}`}>
                <div className="menu-bar" onClick={() => {
                    toggleNavbar(!navActive);
                    props.navActive(!navActive);
                }} >
                    <FontAwesomeIcon icon={faBars} color={"white"} size={"lg"} />
                </div>
                <div className={`menu-button${menuActive === "mainMenu" ? " active" : ""}${!navActive ? " hide" : ""}`} onClick={() => { setMenuActive("mainMenu"); props.selectNavMenu("mainMenu") }} >
                    <span>
                        <FontAwesomeIcon icon={faThLarge} color={"white"} size={"sm"} />
                    </span>
                    <div className="menu-name">เมนู</div>
                </div>

                <div className={`menu-button${menuActive === "onlineMenu" ? " active" : ""}${!navActive ? " hide" : ""}`} onClick={() => { setMenuActive("onlineMenu"); props.selectNavMenu("onlineMenu"); }}>
                    <span>
                        <FontAwesomeIcon icon={faMobileAlt} color={"white"} size={"sm"} />
                    </span>
                    <div className="menu-name">ออนไลน์</div>
                </div>

                <div className={`menu-button${menuActive === "historyMenu" ? " active" : ""}${!navActive ? " hide" : ""}`} onClick={() => {
                    setMenuActive("historyMenu");
                    props.selectNavMenu("historyMenu");
                }}>
                    <span>
                        <FontAwesomeIcon icon={faHistory} color={"white"} size={"sm"} />
                    </span>
                    <div className="menu-name">ประวัติ</div>
                </div>

                <div className={`emp-status ${!navActive ? "hide" : ""}`}>
                    <span>พนักงาน:</span>
                    <span>ฺBeedle EiEi</span>
                    <span>จุดขาย: POS 1</span>
                    <span>Online: 1</span>
                </div>
            </div>
        </>
    )
}

export default Header;