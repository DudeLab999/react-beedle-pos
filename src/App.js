import './App.css';
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { Login, Home, Main } from './components';

export default function App() {

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/home" render={(props) => <Home {...props} />} />
        <Route path="/main" render={(props) => <Main {...props} />} />
        <Route path="*">
          <Redirect to='/' />
        </Route>
      </Switch>
    </Router>
  );
}
